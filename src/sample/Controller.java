package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    public AnchorPane anchorPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        WebView webView = new WebView();
        webView.getEngine().setJavaScriptEnabled(true);

        webView.getEngine().getLoadWorker().stateProperty().addListener(
                new ChangeListener<Worker.State>() {
                    public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState) {
                        if (newState == Worker.State.SUCCEEDED) {
                            webView.getEngine().executeScript("Plotly.plot( \"test\", [{ x: [1, 2, 3, 4, 5], " +
                                    "y: [1, 2, 4, 8, 16] }], { margin: { t: 0 } } );");
                        }
                    }
                });
        webView.getEngine().loadContent("<html><head><script id=\"plotly\" src=\"https://cdn.plot.ly/plotly-latest.min.js\"></script></head><body><p>test</p><div id=\"test\" " +
                "style=\"width:500px;height:500px;\"></div></body></html>");
        anchorPane.getChildren().add(webView);
    }
}
